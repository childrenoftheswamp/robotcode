#include <iostream>
#include <ros.h>
#include <ros/service_client.h>

#include <std_msgs/String.h>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Quaternion.h>

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>

#include <WPILib.h>
#include <cmath>
#include <tf/transform_broadcaster.h>
#include <tf/tfMessage.h>
#include <tf/tf.h>
#include <ros/time.h>
#include <std_srvs/Empty.h>
#include <math.h>
/**
 * This is a demo program showing how to use Mecanum control with the
 * MecanumDrive class.
 */
double robotX;
double robotY;

double cmd_linear;
double cmd_angular;

static void odomCb(const nav_msgs::Odometry& received_msg) {
	//		std::cout << "Received x and y: " << received_msg.pose.pose.position.x
	//				<< ", " << received_msg.pose.pose.position.y << std::endl;
	robotX = received_msg.pose.pose.position.x;
	robotY = received_msg.pose.pose.position.y;
}

static void cmdVelHandler(const geometry_msgs::Twist& received_msg) {
	cmd_linear = received_msg.linear.x;
	cmd_angular = received_msg.angular.z;
}

class Robot: public frc::SampleRobot {
public:

	Robot() {
		std::cout << ("started robot") << std::endl;
		gyro.Calibrate();
		gyro.Reset();
		std::cout << ("connecting") << std::endl;

		nh.initNode(rosSrvrIp);
//		nh.subscribe(odomSub);
		nh.subscribe(cmdSub);
//		nh.advertise(odomOut);
		nh.advertise(accelPub);

		tfBr.init(nh);
		nh.serviceClient(reset_service);

		std::cout << ("subscribed") << std::endl;

	}
	int mode = 0;
	void OperatorControl() override {
		int loops = 0;
		std_srvs::EmptyRequest ereq;
		std_srvs::EmptyResponse eres;

//		reset_service.call(ereq, eres);
		std::cout << ("main loop") << std::endl;

		while (IsEnabled() && IsOperatorControl()) {
			// Zach button based drive selector
			if (m_stick.GetRawButton(7))
				mode = 1;
			if (m_stick.GetRawButton(8))
				mode = 0;
			if (mode == 1) {
				if (m_stick.GetRawButton(4))
					Tdrive(.25, .25);
				else if (m_stick.GetRawButton(1))
					Tdrive(-.25, -.25);
				else if (m_stick.GetRawButton(3))
					Tdrive(-.5, .5);
				else if (m_stick.GetRawButton(2))
					Tdrive(.5, -.5);
				else {
					Tdrive(0, 0);
				}
			}
			if (mode == 0) {
				if (!m_stick.GetRawButton(1)) {
					Tdrive(m_stick.GetRawAxis(5), m_stick.GetRawAxis(1));
				} else {
					float speed_wish_right = (cmd_angular*1)/2 + cmd_linear;
					float speed_wish_left = cmd_linear*2-speed_wish_right;
					Tdrive(speed_wish_right, speed_wish_left);

				}
			}
			heading = (gyro.GetAngle() * M_PI) / 180;
			ros::Time current_time = nh.now();
			tab->PutNumber("heading", heading);

			geometry_msgs::Quaternion odom_quat = tf::createQuaternionFromYaw(
					-1 * heading);

			imuMsg.header.stamp = current_time;
			imuMsg.header.frame_id = "imu_link";
			imuMsg.angular_velocity.z = gyro.GetRate() * M_PI / -180;
			imuMsg.angular_velocity.x = 0;
			imuMsg.angular_velocity.y = 0;

			imuMsg.orientation = odom_quat;

			imuMsg.linear_acceleration.x = accel.GetZ() * 9.8;
			imuMsg.linear_acceleration.y = accel.GetY() * 9.8;
			imuMsg.linear_acceleration.z = accel.GetX() * 9.8;
			// row major x y z
			imuMsg.linear_acceleration_covariance[0] = 0.00146275154;
			imuMsg.linear_acceleration_covariance[4] = 0.00146275154;
			imuMsg.linear_acceleration_covariance[9] = 0.00146275154;

//			imuMsg.orientation.z = odom_quat;

			if (loops % 10 == 0) {
				accelPub.publish(&imuMsg);
			}
			nh.spinOnce();
//			std::cout<<outBuff.str();
//			outBuff.clear();
			sleep(0.01);
			loops++;
		}
	}

	void Tdrive(double Right, double Left) {
		RightMotor.SetSpeed(-Right);
		LeftMotor.SetSpeed(Left);
	}

	void Disabled() override {
		while (IsDisabled()) {
//			std::cout<<"encoder left "<< LeftMotor.GetRaw()<<std::endl;
//			std::cout<<"x: "<<x<<"y: "<<y<<std::endl;
			sleep(0.01);
			nh.spinOnce();
		}

	}
	void Autonomous() override {
	}

private:
	std::shared_ptr<NetworkTable> tab = NetworkTable::GetTable("swamptab");
	char *rosSrvrIp = "10.9.17.95:5802";

	ros::NodeHandle nh;
//	ros::Subscriber<nav_msgs::Odometry> odomSub { "/rtabmap/odom", odomCb };
	ros::Subscriber<geometry_msgs::Twist> cmdSub { "/cmd_vel",
			cmdVelHandler };
	nav_msgs::Odometry odom { };

	frc::Joystick m_stick { 0 };
	frc::VictorSP RightMotor { 9 };
	frc::VictorSP LeftMotor { 8 };
	frc::Compressor comp { 1 };
	frc::DoubleSolenoid shifter1 { 0, 1 };
	frc::DoubleSolenoid shifter2 { 2, 3 };
	frc::ADXRS450_Gyro gyro { }; // use default onboard port
	tf::TransformBroadcaster tfBr;
	ros::ServiceClient<std_srvs::EmptyRequest, std_srvs::EmptyResponse> reset_service {
			"rtabmap/reset_odom" };
	BuiltInAccelerometer accel { BuiltInAccelerometer::Range::kRange_8G };
	sensor_msgs::Imu imuMsg { };
	ros::Publisher accelPub { "/swampbot/imu", &imuMsg };
	double unitsAxisWidth = 0.657200000000000; // offset
	double WEELBASE_WIDTH = 1; // width in meters
	double ODOM_OFFSET = 0.0025;
	int last_enc_left;
	int last_enc_right;
	double odomX;
	double odomY;
	double heading = 0;
	double lastLeftEnc;
	double lastRightEnc;
	const double right_enc_per_meter = 29955.9 * 0.5698507832798019;
	const double left_enc_per_meter = 30783.7 * 0.5698507832798019;
	const double WHEEL_RADIUS = 1;
};

START_ROBOT_CLASS(Robot)
